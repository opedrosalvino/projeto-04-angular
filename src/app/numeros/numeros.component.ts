import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-numeros',
  templateUrl: './numeros.component.html',
  styleUrls: ['./numeros.component.css']
})
export class NumerosComponent implements OnInit {
  lista: number[] = [];

  constructor() {}

  ngOnInit() {}

  gerarNumeroAleatorio() {
    let numero = Math.random() * (9999 - 1);
    this.lista.push(numero);

    return this.lista;
  }
}
